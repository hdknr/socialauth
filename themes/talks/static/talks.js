$(document).keydown(function(e) {
    switch (e.keyCode) {
        case 37:// <-
            url =  $("a#page-prev").attr('href');
            if(typeof(url)=='string' && url.length >0 ){
                $("body").animate({"opacity":0},300,function(){
                    window.location.href =  url;
                });
            }
            break;
        case 38:// Up
            $('html,body').animate({scrollTop:0}, 'fast');
            break;
        case 39:// ->
            url =  $("a#page-next").attr('href');
            if(typeof(url)=='string' && url.length >0 ){
                $("body").animate({"opacity":0},300,function(){
                    window.location.href =  url;
                });
            }
            break;

        case 73:    // I (shift-i)
            $("#popup-image").trigger("click");
            break;
    }
});

$(document).ready(function(){

    $("#popup-image").fancybox({
                padding: 0,
                openEffect : 'elastic',
                closeEffect : 'elastic',
                closeClick : true,
    });

    $(".figure").fancybox({
                padding: 0,
                openEffect : 'elastic',
                closeEffect : 'elastic',
                closeClick : true,
    });

    $("#popup-image").trigger("click");
});
