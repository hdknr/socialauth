==============================================
データモデル
==============================================

.. contents::
    :local:
    :depth: 3
    :class: talks-contents

UserSocialAuthMixin
================================

.. autoclass:: social_auth.db.base.UserSocialAuthMixin
    :members:

UserSocialAuth
================

.. autoclass:: social_auth.db.django_models.UserSocialAuth
    :members:

システムユーザーの持っているソーシャルアカウントは？
------------------------------------------------------

.. code-block:: python

    >>> from django.contrib.auth.models import User
    >>> for u in User.objects.all():
    ...    print u.username, ":",u.social_auth.all()
    ... 
    admin : []
    hdknr : []
    hdknra83b72298d9b4d8f : [<UserSocialAuth: hdknra83b72298d9b4d8f - Github>]

あるいは、

.. code-block:: python

    >>> from django.contrib.auth.models import User
    >>> from social_auth.db.django_models import *
    >>> UserSocialAuth.get_social_auth_for_user(User.objects.all()[2])
    [<UserSocialAuth: hdknra83b72298d9b4d8f - Github>]
    

指定したプロバイダのソーシャルアカウントは?
-----------------------------------------------

同一プロバイダでも、複数アカウントがあり得るので注意。
複数のソーシャルアカウントがあったら、UIで選択させるべき。

.. code-block:: python

    >>> from django.contrib.auth.models import User
    >>> from social_auth.backends.contrib.github import GithubBackend
    >>> User.objects.get(username='hdknra83b72298d9b4d8f').social_auth.filter(provider=GithubBackend.name)[0]
    <UserSocialAuth: hdknra83b72298d9b4d8f - Github>

user_id がわかっているのであれば

.. code-block:: python

    >>> from social_auth.models import UserSocialAuth
    >>> UserSocialAuth.get_social_auth('github',104647)
    <UserSocialAuth: hdknra83b72298d9b4d8f - Github>

    >>> UserSocialAuth.get_social_auth('github',104647).user.username
    u'hdknra83b72298d9b4d8f'

アクセストークンは？
-----------------------------------------------

UserSocialAuth.extra_data は JSONField なので、dictとしてアクセス可能。
アクセストークンの名称はプロバイダごとに違うので注意。
OAuth2のcodeフローの場合はトークンエンドポイントの戻りのJSONが入っている(ハズ.あとでdjango-social-authを調べる )
ので、 "access_token" で参照します。

    - http://tools.ietf.org/html/rfc6749.html#section-4.1.4

.. code-block:: python

    >>> from django.contrib.auth.models import User
    >>> from social_auth.backends.contrib.github import GithubBackend
    >>> User.objects.get(username='hdknra83b72298d9b4d8f').social_auth.filter( provider=GithubBackend.name )[0].extra_data['access_token']
    u'970851616caf01ca02b96c99965cc9cc19d6b610'

