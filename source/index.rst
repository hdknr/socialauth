.. Social Django documentation master file, created by
   sphinx-quickstart on Sat Aug 25 03:19:32 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===============================================
django-social-authでOAuthに触れてみる
===============================================

.. note::
    - `2012 Pythonアドベントカレンダー(Webフレームワーク) <http://connpass.com/event/1439/>`_ の #17 の記事になります
    - `+hdknr <https://plus.google.com/u/3/111834161848985316983/posts>`_ が書きました。
    - `@yamionp <http://d.hatena.ne.jp/yamionp/20121216/1355652349>`_ さんからリレーいただきました。
      次は `@aodag さんにリレー <http://aodag.posterous.com/2012-python-18-pastedeploy>`_ されます。

はじめに
==============================================

- デジタルアイデンティティの研究開発のお手伝いを数年やっています。
- 主に、OAuth2( :rfc:`6749` )とその枠組みでデジタルアイデンティティにまつわる標準化、 :term:`OpenID Connect` の研究開発です。
- OAuth2ドラフトからRFCになり、メジャーなサービスが対応してきています。
- Djangoで構築するWebサービスにもOAuth2を対応することで提供するインターネット体験を高めるとかどうでしょうか。

GithubもOAuth2
----------------------

- GithubのAPIを使うだけなら別の :ref:`リクエスト認証手段 <github.auth>` も用意されています。
- が、自サービスのローカルアカウントに対して、ソーシャルアカウントを選択したり、
  複数のソーシャルアカウントを紐づけたりすることが普通になっていくと思います。
- 今後普及の進むと思われるOAuth2を使っておくのがいいのではないでしょうか。

OAuth2とは？
----------------------

- アクセスするために許可の必要なリソースに対するリクエストを :term:`アクセストークン` で認証するルールを定めています。
- アクセストークンをユーザーが許可した代理アクセス先( :term:`クライアント` )に対して、
  認可サーバーで正しく安全に発行する手順(フロー)を定義しています。
- クライアントがアクセスしたいリソースの組を :term:`スコープ` で指示することができます。

django-social-authだと結構簡単
==============================================

- :term:`django-social-auth` は結構簡単にOAuth2を自サービスに導入する事が可能です。
 
    - OAuth2 は簡単なんです

django-social-authの特色
--------------------------

- Djangoの :term:`認証バックエンド` の仕組みで、 :term:`認可応答` を処理し、ソーシャルアカウントでDjangoの認証が出来るようになっています
        
    - 新規登録もほぼ自動でできます
    - 既にログインされているユーザーにソーシャルアカウントを紐づけられます
    - ソーシャルアカウントで既存のユーザーとしてログインできます

- つまり、各認可サーバー(Twitter,Facebook,Github,....) に対してプラグインできるようになっています

    - 多くの :term:`バックエンドコントリビューション` があります

- 認可バックエンドで必要とされる処理を :doc:`pipeline` を定義する事でカスタマイズできます  

    - :doc:`partial.pipeline` を使う事で、アプリケーションで必要なUIを間に挟むこともできます

django-social-authの導入
--------------------------

- インストールは簡単

    ::

        $ pip install django-social-auth

- サイトへの組み込みは以下の手順でおこないます

    - :doc:`github.reg` でサイトを登録する
    - :doc:`social_auth.conf` して、OAuthを使えるようにする
    - 必要であれば :doc:`partial.pipeline` を実装して、ユーザーに認証前のUIを提供する
    - 所得されたアクセストークンでサーバーからデータを所得して、認証ユーザーに対してサービスを提供する

サンプル:Gistを操作する
==============================================

- django-social-authだと Accesds Tokenの取得までが簡単にできます。
- Access Tokenさえ取得してしまえば、あとはGithubのAPIに乗っ取って操作するだけです。
- Githubは OAuth2のリソースアクセス手段であるベアラトークン( :rfc:`6750` ) によりリクエスト認証を行う事ができます。
- つまり、HTTPの **Authorization** ヘッダーに以下のようにAccess Tokenを入れればいいです。

    ::

        Authorization: Bearer 23dbb1c3c4a23646cbcf640ed9224a95cce25fae 

    - GithubではURLのクエリパラメータにアクセストークンを指定するなど、別の手順も用意されてはいます
    - クエリパラメタにトークンを入れる方法は :rfc:`6749` 自体で考慮されている手順です

- Gistの操作自体、このAuthorization ヘッダーとアクセストークンの扱い以外、OAuthは関係ありません。
- しょぼいサンプルコードを https://bitbucket.org/hdknr/socialauth に置いています。

    - このドキュメントを含んでいます

OpenID Connect とは？
==============================================

- ここからは、 :term:`OpenID Connect` の宣伝です。
- OpenID Connectは OAuth2 の認可の仕組みを使って、プライバシーを考慮した上でクラウド上のサービス間でアイデンティティや属性の流通を標準的に行えるように考えられている仕組みです。

アイデンティティ
---------------------

- django-social-authをつかって、Github, Facebook, Twitterなどいろいろアクセスしたり、
  ソースコード読むとソーシャルユーザーの識別子の扱いがまちまちである事に気がつくでしょう。
  (そもそもソーシャルサイトごとに別のバックエンドが必要です)
- OAuth2 ではユーザーのアイデンティティを標準的にやり取りすることをスペックでは定めていません。
- OpenID Connectは(主に)OAuth2の一連のリクエストのやり取りの結果、  :term:`ID Token` という標準化されたデータで
  ソーシャルユーザーのアイデンティティを安全に識別する仕組みを提供しています。

ユーザー属性
---------------------

- 同じく OAuth2 ではユーザー属性の共有の方法が標準化されていません。
- OpenID Connectでは :term:`UserInfo` という仕組みで３者間で共有された認証コンテキストに
  対応するユーザーの属性を同じ手順で共有することができます。

その他
--------

- アイデンティティ証書や属性を暗号化して渡したり、その情報に :term:`非否認性` をもたせたりするために :term:`JSON Web Token` というデータのシリアライズを標準的に行う仕組みがあります。これは :term:`JOSE` ワーキンググループでディスカッションされている JWK,JWS,JWEに基づいています。
- 自サービスに訪れたユーザーが自分のソーシャルアイデンティティの認証元を指定し、そのソーシャルサイトとの間で動的に関係をとれる仕組みが用意されています( :term:`Discovery` + :term:`Dynamic Registration` 。 Dynamic Registrationに関しては OAuth2も別のRFC化に向けてディスカッション中です )。 
- 共有されたアイデンティティ証書(:term:`ID Token`)を元に３者間でセッション管理出来る仕組みがあります( :term:`Session Management` )
- 認証をどのように行うか、その結果のデータの関して認証コンテキストを拡張する要求を行えるようになっています
  ( :term:`リクエストオブジェクト` )。
- リソースがさらに第４者にある場合の考慮もされています
  ( UserInfo : :term:`Aggregated and Distributed Claims` 。OAuth2を基にした :term:`UMA` と言う手順も議論されています)   
 

資料:django-social-authについて
===============================================

.. toctree::
    :maxdepth: 2

    github
    github.reg
    social_auth.conf
    oauth.github
    pipeline
    backends.pipeline
    partial.pipeline
    oauth.authn.rst 
    models
    res

- Indices and tables

    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`

