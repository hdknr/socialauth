==============================================
OAuth : 認証？
==============================================

.. contents::
    :local:
    :depth: 3
    :class: talks-contents

OAuthはサードパーティに認証状態を提供するのではない
=========================================================

- 決められたリソースセット( scope で表現されるRESTエンドポイントセット)
  へアクセスするためのクレデンシャル(アクセストークン)を提供する

- トークンが得られたと言う事は、以下のコンテキストが３者間で共有されている

    - 認可サーバーにおいて、誰かがアイデンティティを認証された
    - 認証された誰かが、リソースを代理人( :term:`Client` )が利用してもよい、と許可した
 
OAuth サーバーアプリケーション（バックエンド)がきめたルールで認証
================================================================================================

- リソースにアクセスすると実際にリソースが返ってきた

    - そのリソースに、「誰か」を表すデータを入れるかどうかは、リソースサーバー次第

OpenID Connect : 3者間での認証ルールの取り決め
--------------------------------------------------------------------


