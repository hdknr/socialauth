==============================================
Github API
==============================================

.. contents::
    :local:
    :depth: 2
    :class: talks-contents

GithubにOAuthアクセストークンが使えるAPIがあります
=====================================================

- http://developer.github.com/v3/

Activity - ソーシャルコーディングでの活動状態
--------------------------------------------------

- http://developer.github.com/v3/activity/


Gists -
----------------------------------------------------

- http://developer.github.com/v3/gists/

Database API - データの操作 
----------------------------------------------------

- http://developer.github.com/v3/git/

Issues - トラッカー
------------------------------------------------

- http://developer.github.com/v3/issues/


Org - 組織/グループ 管理
------------------------------------------------

- http://developer.github.com/v3/orgs/


Pull Requests -  プル要求
---------------------------------------------

- http://developer.github.com/v3/pulls/


Repository - レポジトリ操作
---------------------------------------------

- http://developer.github.com/v3/repos/

User - ユーザー操作
---------------------------------------------

- http://developer.github.com/v3/users/

Search - 検索
---------------------------------------------

- http://developer.github.com/v3/search/

Markdown - Markdown文書をレンダリングしてもらう
-----------------------------------------------------

- http://developer.github.com/v3/search/


.. _github.auth:

リクエスト認証
==================

- http://developer.github.com/v3/#authentication


.. _github.auth.basic:

基本認証
----------

- いわゆるBasic Auth です。

OAuth
------

- http://developer.github.com/v3/oauth/

OAuth2:トークン(Authorization Header )
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- 基本的にはこれを使いましょう。
- :term:`OAuth Bearer` トークンです。
- "Bearer"の代わりに "token"でもOKです。

- :ref:`github.auth.basic` との違いはリクエストヘッダーの書式と、そこに指定されるクレデンシャルの意味の違いだけです。 

OAuth2:トークン(access_token クエリパラメータ)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- ご利用は慎重に。

OAuth2:クライアントクレデンシャル
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- ご利用はさらに慎重に。
