==============================================
定義済パイプライン
==============================================

.. contents::
    :local:
    :depth: 2
    :class: talks-contents

social
=====================================================

associate_user : ソーシャルユーザーとシステムユーザーを紐付けて、次のパイプラインに渡す
----------------------------------------------------------------------------------------

- 必要であればソーシャルユーザー、ユーザーを新規作成します。

.. autofunction:: social_auth.backends.pipeline.social.associate_user


social_auth_user : 既存ソーシャルユーザーとシステムユーザーを次のパイプラインへ渡す
------------------------------------------------------------------------------------------------

.. autofunction:: social_auth.backends.pipeline.social.social_auth_user

load_extra_data : 取得したリソースからユーザーのextra_dataを取り出してソーシャルユーザーに反映
-----------------------------------------------------------------------------------------------------

- アクセストークンやユーザー識別子、メールアドレスなどその他の情報が含まれます
- バックエンドプロバイダによって異なります。
- よって、実際のデータはJSONで保存されます。( OAuthでは決めていない )

.. autofunction:: social_auth.backends.pipeline.social.load_extra_data

user
=====================================================

get_username : usernameを次のパイプラインへ渡す
--------------------------------------------------------

.. autofunction:: social_auth.backends.pipeline.user.get_username

create_user : ソーシャルユーザーを作成して渡す
--------------------------------------------------------

.. autofunction:: social_auth.backends.pipeline.user.create_user

update_user_details : ユーザー属性を更新する
--------------------------------------------------------

.. autofunction:: social_auth.backends.pipeline.user.update_user_details


associate
=====================================================


associate_by_email:  指定されたメールアドレスに対応するソーシャルユーザーを渡す
---------------------------------------------------------------------------------------------------------

.. autofunction:: social_auth.backends.pipeline.associate.associate_by_email

misc
=====================================================

save_status_to_session: 現在の social-auth 状態をセッションに保持します 
------------------------------------------------------------------------

.. autofunction:: social_auth.backends.pipeline.misc.save_status_to_session
