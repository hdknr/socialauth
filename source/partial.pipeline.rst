=======================================================
パーシャルパイプライン
=======================================================

.. contents::
    :local:
    :depth: 3
    :class: talks-contents


django-social-authでのパイプライン処理
=====================================================================

social_auth.views.complete : 認可応答のランディング処理(OAuth redirect_uri)
---------------------------------------------------------------------------------

.. code-block:: python

     from social_auth.decorators import dsa_view

     @csrf_exempt       #: POST で Authorization Response がくる事もあります
     @dsa_view()        #: Social Authの状態を取得 
     def complete(request, backend, *args, **kwargs):                       #:**
         if request.user.is_authenticated():
             #:認証が終わっていたら完了処理へ
             return associate_complete(request, backend, *args, **kwargs)   #:**
         else:
             #:終わっていなかったら、Authorization Responseを元に認証処理へ
             return complete_process(request, backend, *args, **kwargs)     #:**

Social Authの状態取得
---------------------------------------------------------------------------------

.. automodule:: social_auth.decorators
    :members:

social_auth.views.auth_complete:パイプラインを逐次処理して認証完了させる
----------------------------------------------------------------------------

.. code-block:: python

    def auth_complete(request, backend, user=None, *args, **kwargs):    #: **
        if user and not user.is_authenticated():
            user = None
    
        if request.session.get(PIPELINE_KEY):
            #:パイプライン途中であれば、パイプライン状態を取得
            data = request.session.pop(PIPELINE_KEY)
            idx, xargs, xkwargs = backend.from_session_dict(data, user=user,
                                                            request=request,
                                                            *args, **kwargs) #:**

            #:対象のバックエンドのパイプラインであれば次のパイプラインを実行
            if 'backend' in xkwargs and \
               xkwargs['backend'].name == backend.AUTH_BACKEND.name:
                return backend.continue_pipeline(pipeline_index=idx,
                                                 *xargs, **xkwargs)     #:**

        #:パイプライン完了
        return backend.auth_complete(user=user, request=request, *args, **kwargs) #:** 

settings.SOCIAL_AUTH_PIPELINE_RESUME_ENTRY : 再開パイプラインエントリを強制
--------------------------------------------------------------------------------

- :ref:`save_status_to_session`


実際の流れパイプラインの流れ
=====================================================================

認可応答(Authorization Response)のパラメータをセッションに保持する
---------------------------------------------------------------------

.. _save_status_to_session:

social_auth.backends.pipeline.misc.save_status_to_sessionを通す
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- オリジナルのAuthorization ResponseがDjangoセッションに保持されるので、
  以後のパイプラインでは、WSGI Request
  GET(やPOST)のパラメータに :term:`Authorization Response` 
  のパラメータが含まれていないのであれば、セッションから取得する。

  以下の例だと、 

  .. code-block:: python
    
        SOCIAL_AUTH_PIPELINE = (
        
            'social_auth.backends.pipeline.social.social_auth_user',
            'social_auth.backends.pipeline.misc.save_status_to_session',
        
            #:以後のPipelineではセッションの中のAuthReqをアクセスできる.....
        
        )

- :ref:`social_auth.backends.pipeline.misc.save_status_to_session`

アプリケーションで何らかゲート処理を行うパイプラインを用意
-----------------------------------------------------------------------------

- セッションでフラグを管理する単純なゲートパイプライン

.. code-block:: python

    def pipeline_binding(*args,**kwargs): #:**

        request = kwargs.get('request',None)
        assert request != None
    
        last = request.session.get('last',None)     #: フラグ確認
        if last != None and (now() - last ).total_seconds() < 100:
            #:フラグたっているので....
            return None  #: 次のパイプラインへ
    
        #:フラグたてる
        request.session['last'] = now()

        #:HTMLページを表示
        return render_to_response( 'github/binding.html',
                { 
                    'is_authenticated': request.user.is_authenticated(),
                } , 
                context_instance=template.RequestContext(request),) 
    
パイプラインに追加
-----------------------------------------------------------------------------

.. code-block:: python

    SOCIAL_AUTH_PIPELINE = (
    
        'social_auth.backends.pipeline.social.social_auth_user',
        'social_auth.backends.pipeline.misc.save_status_to_session',
    
        'app.github.views.pipeline_binding',                #: これを挟むとUIで分岐
    
        'social_auth.backends.pipeline.user.get_username',
        'social_auth.backends.pipeline.user.create_user',
        'social_auth.backends.pipeline.social.associate_user',
        'social_auth.backends.pipeline.social.load_extra_data',
        'social_auth.backends.pipeline.user.update_user_details',
    )

パイプラインに戻るには再度 AuthResのURLにアクセスするとデコレータがパイプライン処理を再開
---------------------------------------------------------------------------------------------

- socialauth_complete のURLに戻す

.. code-block:: html

    {% extends "base.html" %}
    {% block content %}
    <h1>Binding Your Social Identity</h1>
    <a href="{% url socialauth_complete backend='github' %}" > 了承する</a>
    {% endblock %}

パイプラインが終わると settings.LOGIN_REDIRECT_URLへ
------------------------------------------------------------------


