================
リソース
================

.. contents::
    :local:
    :depth: 2
    :class: talks-contents

Todo
=====

- トークンについて 

    - http://django-social-auth.readthedocs.org/en/latest/tokens.html

- シグナル

    - http://django-social-auth.readthedocs.org/en/latest/signals.html
    - これはいいかな

- リダイレクト先のURL

    - http://django-social-auth.readthedocs.org/en/latest/configuration.html

- コンテキストプロセッサー

    - http://django-social-auth.readthedocs.org/en/latest/configuration.html

- その他

    - Configureにいろいろ

リンク
======

.. glossary::

    django-social-auth
    django-social-auth Github
        http://django-social-auth.readthedocs.org/en/latest/backends/github.html

    OAuth HTTP MAC
        http://tools.ietf.org/html/draft-ietf-oauth-v2-http-mac-01

    OAuth
        - :rfc:`6749`

    OAuth Bearer
        - :rfc:`6750`
        - 「運び屋」トークンです。
        - お家の鍵と同じです。鍵が合えば、部屋に入る事ができます。たとえあなたの部屋であっても、彼女に鍵を渡す(複製する)
          と部屋には入る事ができます。
        - つまり、ベアラトークンでは鍵を使う人が「鍵を使うにふさわしい人である」保証を前提としています。
        - このようなトークンとは別に、Holder-of-Key ( 「保持者専用鍵」) トークン と言うのも考えられて他のプロトコルでは定義されています。( SAMLなど )
        - 「保持者専用鍵」トークンは、挿入した部屋の鍵が挿入しようとした人専用の鍵でなければ挿入してドアを開ける事はできません。
        - 「保持者専用鍵」トークンが上手く行く環境を前提とすると閾が高すぎるのでベアラトークンを採用しています。

    requests
        http://docs.python-requests.org/en/latest/user/quickstart/  

    クライアント
    Client
        Resource Ownerに代わってResourceをアクセスし、
        何らかのサービスを Resource Ownerに提供する
        エンティティ。

        例えば、Twitterに定期的に投稿したり、メンションを検知したりするBotアプリケーションとか。

    認可リクエスト
    認可要求
    Authorization Request
        認可要求 。 アプリケーションから OAuth のプロセスを開始するHTTP(S)要求です。

    認可応答
    認可レスポンス
    Authorization Response
        認可応答。 Authorization Serverで、Resource Owner がトークンの発行を許可した場合、
        その旨を :term:`Client` に通知するパラメータ。

        redirect_uri に戻されます。

    クライアント登録
    Client Registration
        - 認可サーバーに対してアプリケーション登録を行い、クレデンシャルを受け取ります。
        - http://tools.ietf.org/html/rfc6749.html#section-2

    redirect_uri
        - :term:`Authorization Request` に指定されるURLで、ここに認可サーバーから :term:`Authorization Response` が戻されます。
        - 指定されない場合、  :term:`クライアント登録` で登録されたURLに戻されます。
        - `不正に認可コードを取得させない <http://tools.ietf.org/html/rfc6749.html#section-10.6>`_ ように考慮されています。 

    コードフロー
        - 認可(Grant)を得る為に Authorization Code を用いる方法
        - http://tools.ietf.org/html/rfc6749.html#section-4.1

    OpenID Connect
        - http://openid.net/connect/

    クライアント認証
        - 認可サーバーを :term:`クライアント登録` で発行したクレデンシャルで、 :term:`クライアント`  を認証します。
        - http://tools.ietf.org/html/rfc6749.html#section-2.3
        - `トークンエンドポイント` では認証が必要です ( http://tools.ietf.org/html/rfc6749.html#section-3.2.1 )

    トークンエンドポイント
    Token Endpoint
        - トークンを発行するREST呼び出し先URLです
        - トークンを発行するには :term:`クライアント認証` が必要です。
        - http://tools.ietf.org/html/rfc6749.html#section-3.2

    アクセストークン
        - リソースをアクセスする為に必要なクレデンシャルです。

    スコープ
    Scope
    Access Token Scope
        - :term:`アクセストークン` が適用されるリソースの組を表します。 
        - http://tools.ietf.org/html/rfc6749.html#section-3.3

    ID Token
        - アイデンティティを一意に示す属性データ(クレームといいます)セット 
        - JSON形式で構成されて、 :term:`JSON Web Token` にシリアライズされて交換されます
        - http://openid.net/specs/openid-connect-messages-1_0.html#id_token

    JWT
    JSON Web Token
        - JSON形式に格納された属性データを暗号化したりデジタル署名を付与したりして安全に交換されるためのシリアライズする仕組み
        - http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-05

    UserInfo
        - :term:`ID Token` で示されたアイデンティティの属性データ
        - http://openid.net/specs/openid-connect-messages-1_0.html#userinfo_ep 

    JOSE
        - Javascript Object Signing and Encryption (jose) Working Group 
        - http://datatracker.ietf.org/wg/jose/

    Discovery
        - OpenIDプロバイダの場所や、様々な属性を取得する仕組み
        - http://openid.net/specs/openid-connect-discovery-1_0.html

    
    Dynamic Registration
        - :term:`クライアント` をOpenIDプロバイダに登録する手順
        - http://openid.net/specs/openid-connect-registration-1_0.html
        - OAuthのDynamic Registraton : http://tools.ietf.org/html/draft-oauth-dyn-reg-v1-03 

    Session Management
        - http://openid.net/specs/openid-connect-session-1_0.html

    Aggregated and Distributed Claims 
        - http://openid.net/specs/openid-connect-messages-1_0.html#anchor14

    UMA
        - http://tools.ietf.org/html/draft-hardjono-oauth-umacore-05

    非否認性
        - http://ja.wikipedia.org/wiki/%E5%90%A6%E8%AA%8D%E4%B8%8D%E5%8F%AF

    認証バックエンド
        - https://docs.djangoproject.com/en/dev/ref/authbackends/

    バックエンドコントリビューション
        - http://django-social-auth.readthedocs.org/en/latest/backends/index.html

    Request Object
    OpenID Request Object
    リクエストオブジェクト
        - http://openid.net/specs/openid-connect-messages-1_0.html#OpenID_Request_Object
