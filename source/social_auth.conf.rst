==============================================
django-social-auth 設定
==============================================

.. contents::
    :local:
    :depth: 4
    :class: talks-contents

インストール : pip install django-social-auth
======================================================

::

    $ pip install django-social-auth

settings.py : Githubのアクセストークンを取得するまで最低以下の設定が必要です
============================================================================================

INSTALLED_APPS
------------------

.. code-block:: python

    INSTALLED_APPS += ('social_auth',)

AUTHENTICATION_BACKENDS
------------------------------------------------------

- 認証コンテキストをシステムユーザーに紐づけてログインユーザーとするのに、今回はGithubをつかうので GithubBackend を追加します

.. code-block:: python 

    AUTHENTICATION_BACKENDS = ( 
        'social_auth.backends.contrib.github.GithubBackend',
    )+ global_settings.AUTHENTICATION_BACKENDS

Githubバックエンド: social_auth.backends.contrib.github
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

social_auth.backends.contrib.github.GithubBackend
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. autoclass:: social_auth.backends.contrib.github.GithubBackend
    :members:

social_auth.backends.contrib.github.GithubAuth
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. autoclass:: social_auth.backends.contrib.github.GithubAuth
    :members:

Webアプリのクレデンシャル
------------------------------------------

- :ref:`github.reg.registration` で登録したクレデンシャルを設定します。

.. code-block:: python

    GITHUB_APP_ID      = os.environ.get('GITHUB_CLIENT_ID','__TODO__IN_WSGI.PY__')
    GITHUB_API_SECRET  = os.environ.get('GITHUB_CLIENT_SECRET','__TODO__IN_WSGI.PY__')


urls.py  : social_auth.urls を設定
=====================================================

.. code-block:: python

    from django.conf.urls import patterns, include, url
    
    urlpatterns = patterns('',
        url(r'social/', include('social_auth.urls')),                           #: django-social-auth フレームワーク
        url(r'', 'app.github.views.default',name='app_github_default' ),
    )

socialauth_begin : 認可リクエストURLを作成
-------------------------------------------------

- backend引数に認証バックエンドの名称を指定します

.. code-block:: html

      <a href="{% url socialauth_begin backend='github' %}">Github</a>

- 以下のようにレンダリングされます

.. code-block:: html

     <a href="/github/social/login/github/">Github</a>

- これを践むとLocationでGithubの認可エンドポイントに飛びます

    :: 

        https://github.com/login/oauth/authorize?
        scope=gist&
        state=fZ2vTfpxtWUtwQr4g1F4XNocg6YLQp5g&
        redirect_uri=http%3A%2F%2Fapp.lafoglia.jp%3A9990%2Fgithub%2Fsocial%2Fcomplete%2Fgithub%2F%3Fredirect_state%3DfZ2vTfpxtWUtwQr4g1F4XNocg6YLQp5g&
        response_type=code&
        client_id=cede527bcc088ff7203f

    - scope: トークンでアクセスしたいOAuthのリソースの内容を伝えます。Gistの更新系リソースを使うには "gist"が必要です
    - state: このパラメータが認可応答にそのまま帰ります。
    - redirect_uri : このURLに認可応答が変えります。 Githubは :ref:`アプリケーション登録 <github.reg.registration>` で指定した、"Callback URL"がこのURLに部分一致していないとエラーでトークンを発行しません。
    - response_type : OAuthのフローを決めます。ここではコードフローです。
    - client_id : クライアント登録したWebアプリケーションのIDです。


socialauth_complete : 認可応答を受けてパイプライン処理を行う
--------------------------------------------------------------------------------------------------

- 認可応答が帰ります

    ::

        http://app.lafoglia.jp:9990/github/social/complete/github/?
        code=e648f11d44fd6429bfa1&
        redirect_state=fZ2vTfpxtWUtwQr4g1F4XNocg6YLQp5g&
        state=fZ2vTfpxtWUtwQr4g1F4XNocg6YLQp5g

    - code: アクセストークンが発行された「受付番号」です。
    - state : 認可リクエストで指定したパラメータが戻ってきています
    - redirect_state : django-social-authが redirct_uriの一部として指定した拡張パラメータがそのまましていされています

- Webアプリケーションでは settings.SOCIAL_AUTH_PIPELINE でしていした「 :doc:`パイプライ処理 <pipeline>` 」を逐次行って行きます。

- パイプラインが処理される事で、

    - codeを元にGithubにアクセストークンをもらいに行きます
    - ユーザーが作成、あるいは既存のユーザーとして認証されます
    - アクセストークンがもらえるので、バックエンドの処理次第ですが、データを取得するなりしてユーザー情報を設定します

- 最後に、settings.LOGIN_REDIRECT_URLへナビゲートされるので、Webアプリケーションは認証された(登録された)エンドユーザーにサービスを提供します。

    - パイプラインを中断して画面を挟んだりするには「 :doc:`partial.pipeline` 」の実装を行います。



settings.SOCIAL_AUTH_COMPLETE_URL_NAME : 別のビューでカスタム処理したい、とか名前の問題があったら
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- デフォルト = 'socialauth_complete'

settings.SOCIAL_AUTH_ASSOCIATE_URL_NAME : 新しくソーシャルアカウントとリンクした時のURL
--------------------------------------------------------------------------------------------------------

- ソーシャルアカウントとのリンクがされた時の処理
- デフォルト =  'socialauth_associate_complete'


