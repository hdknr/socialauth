====================================================
OAuth アクセストークンで Github API(Gist)を利用
====================================================

.. contents::
    :local:
    :depth: 3
    :class: talks-contents

Github OAuth オーソライゼーションリクエスト
================================================

- http://developer.github.com/v3/oauth

Github OAuth リソースリクエスト
======================================

:rfc:`6749` を参照してください

Authorization: Bearer {{access_token}} 
---------------------------------------------------

- HTTPリクエストヘッダーにトークンを設定してリクエストします

GET パラメータ ?access_token={{access_token}}
---------------------------------------------------

Gistのリソースを使う
====================================================================================

Gsst取得:requests を使ってアクセスしてみる
------------------------------------------------------------------------------------

:term:`requests` を使って自分のGistsの一覧を取得してみる。30件見つかっている。

.. code-block:: python

    (social)hdknr@wzy:~/ve/social/src/socialauth/sample$ python manage.py shell
    Python 2.7.3rc2 (default, Apr 22 2012, 22:30:17) 
    [GCC 4.6.3] on linux2
    Type "help", "copyright", "credits" or "license" for more information.
    (InteractiveConsole)
    >>> import requests
    >>> endpoint = 'https://api.github.com'
    >>> path = '/gists'
    >>> import os
    >>> headers = {'Authorization': 'token %s' % os.environ['GITHUB_TEST_TOKEN'] }
    >>> res = requests.get( endpoint + path , params={}, headers=headers )
    >>> len(res.json)
    30
    >>> print res.json[0]['description']
    サーバー証明書の内容をTEXTで。


Gist作成:POST /gists で {u'message': u'Not Found'} が返る
----------------------------------------------------------------

- scope に "gist" が足りないのです。
- settings.GITHUB_EXTENDED_PERMISSIONS に追加のスコープを指定します。

    .. code-block:: python

        GITHUB_EXTENDED_PERMISSIONS = ["gist",]  

.. code-block:: python

    >>> import requests
    >>> endpoint = 'https://api.github.com'
    >>> path = '/gists'
    >>> import os
    >>> headers = {'Authorization': 'token %s' % os.environ['GITHUB_TEST_TOKEN'] }

以上は取得するのと同じ。作成の場合は、JSONデータを作って、postします。

.. code-block:: python

    >>> data={'files':{'mycode.py': {'content':'import sys'} } }
    >>> import json
    >>> jdata = json.dumps(data)
    >>> res=requests.post(endpoint+path, jdata,headers=headers)
    >>> res
    <Response [201]>

.. image:: _static/oauth.github/created.gist.png 

