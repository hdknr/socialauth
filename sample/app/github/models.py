from django.db import models
from django.contrib.auth.models import User

from api import Gist as GistApi, get_social_user

class Gist(models.Model):
    user    = models.ForeignKey(User)
    gid     = models.CharField(u'Gist ID',max_length=100,null=True,blank=True,default=None)
    filename = models.CharField(u'File Name',max_length=100)
    contents = models.TextField(u'Contents') 
    response = None

    def save(self,*args,**kwargs):
        ''' '''
        if self.gid == None:
            ''' create '''
            client = GistApi(social_user = self.social_auth )
            client.add_file(self.filename,self.contents )
            self.response = client.create()

    @property
    def social_auth(self):
        return self.user and get_social_user(self.user) 

    class Meta:
        abstract=True
