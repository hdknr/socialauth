# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from views import *

urlpatterns = patterns('',
    url(r'social/logout', 'app.github.views.logout', name='app_github_logout'),
    url(r'social/', include('social_auth.urls')),                           #: django-social-auth フレームワーク
    url(r'gist/(?P<gid>\d*)(?P<command>.*)', GistView.as_view(), name="app_github_gist"),
    url(r'', 'app.github.views.default',name='app_github_default' ),
)

