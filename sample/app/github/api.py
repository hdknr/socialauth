# -*- coding: utf-8 -*-

from social_auth.backends.contrib.github import GithubBackend
from social_auth.models import UserSocialAuth

import requests
import json

def context(request):
    return  { 'github_user'  : get_social_user(request.user), }

def get_social_user(sys_user=None,social_id = None,provider=GithubBackend.name ):
    ''' UserSocialAuth の取得　'''
    if social_id:
        ret = UserSocialAuth.get_social_auth(GithubBackend.name, social_id )
        return ret

    if sys_user == None  or sys_user.is_authenticated() == False:
        return None

    ret = sys_user.social_auth.filter( provider=provider )
    return ret[0] if len(ret) > 0 else None

def get_access_token(sys_user=None,social_id = None, social_user = None):
    ''' アクセストークン　'''
    if social_user == None:
        social_user = get_social_user(sys_user,social_id )
    return social_user.extra_data['access_token']
    

class Api(object):
    ''' Github API 

        - http://developer.github.com/v3/ 

        OAuth 2 Access Token in Header 

        - http://developer.github.com/v3/#authentication
    '''
    ENDPOINT = 'https://api.github.com'
    METHOD="GET"
    params = {}
    def __init__(self,sys_user=None,social_id = None, social_user = None,access_token=None):
        if social_user == None:
            social_user = get_social_user(sys_user,social_id ) 

        self.social_user = social_user
        self.access_token = access_token or get_access_token(social_user = self.social_user )

    def endpoint(self,path):
        ''' 指定したパスでエンドポイントを返す '''
        return  self.ENDPOINT + path

    @property
    def default_headers(self):
        ''' OAuth Bearer Tokenのヘッダー '''
#        return  { 'Authorization' : "token %s" % self.access_token , } #:こっちでも動きます
        return  { 'Authorization' : "Bearer %s" % self.access_token , }

    def get(self,path,headers={},params={} ):
        ''' パスに対して GET を行う '''
        h= self.default_headers
        p= self.params
        h.update(headers)
        p.update(params)

        return requests.get(self.endpoint(path),params=p,headers=h)  

    def post_json(self,path,json_data,headers={}):
        ''' パスに対して データをJSONとして POST する '''
        h= self.default_headers
#        h['Content-Type'] = 'application/json'
#        h['charset'] = 'utf-8'
        h.update(headers)
        return requests.post(self.endpoint(path),
                        data=json.dumps(json_data),headers=h)  

class Gist(Api):
    ''' API Gist '''
    files = {}
    ''' Gistはエントリに対して複数ファイル指定できます '''

    def add_file(self,name,content ):
        ''' ファイルを追加します '''
        self.files[name] = {'content': content }

    def list(self):
        ''' アクセストークンを与えたユーザーの Gistの一覧を取得します '''
        return self.get('/gists')

    def create(self,description="a gist", public=True,):
        ''' Gistを新規に作成します '''
        if len(self.files) < 1:
            return None 

        data ={ 
            "description":  description,
            "public" : public,
            "files" : self.files,
        }

        return self.post_json('/gists',data)
