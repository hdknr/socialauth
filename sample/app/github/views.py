# -*- coding: utf-8 -*-
from django import template
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib import auth


from social_auth.decorators import dsa_view
from django.utils.timezone import now
from datetime import datetime

from models import *
from forms import *

def default(request,*args,**kwargs):
    ''' default page '''
    return render_to_response( 'github/default.html',
        {  
        },
        context_instance=template.RequestContext(request),)

def logout(request):
    ''' logout '''
    auth.logout(request)
    return  HttpResponseRedirect( reverse('app_github_default'))

def pipeline_binding(*args,**kwargs):
    ''' パイプライン: ソーシャルユーザーを受け入れるかどうかの判断
    '''
    request = kwargs.get('request',None)
    assert request != None
    assert kwargs['github'] == True

    user = kwargs.get('user',None)
    social_user = kwargs.get('social_user',None)
    uid = kwargs.get('uid',None)
    is_new = kwargs.get('is_new',None)

    #:ゲート(last)を判定
    last = request.session.get('last',None)
    if last != None and (now() - last ).total_seconds() < 100:
        return None  #:つぎへ

    #:ゲートをセット
    request.session['last'] = now()

    #:表示メッセージ切り替え
    if request.user.is_authenticated():
        message = u"現在のユーザーにバインド"
    elif user == None and social_user ==None:
        message = u"新規登録"
    else:
        message = u"ソーシャルアカウントでログイン"

    #: UI
    return render_to_response( 'github/binding.html',
            { 
                'is_authenticated': request.user.is_authenticated(),
                'is_new': is_new,
                'user':user,  
                'social_user': social_user,
                'uid':uid,
                'message': message,
            } , 
            context_instance=template.RequestContext(request),)
     

from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView


class GistView(FormView):
    template_name = "github/gist/item.html"
    form_class = GistItemForm

    def dispatch(self, request, *args, **kwargs):
        #:
        if kwargs['command'] in [None,'list','']:
            #:一覧
            kwargs['command'] = 'list'
            self.form_class = GistListForm
        else:
            kwargs['command'] = kwargs['command'].replace('/','')
            
        self.template_name = "github/gist/%(command)s.html" % kwargs

        try:
            res =  super(GistView,self).dispatch(request,*args,**kwargs)

            return res
        except Exception,e:
            return self.render_to_response(self.get_context_data(
                        form=self.get_form(self.form_class)
                    ))

    def get_form_kwargs(self):  #:API
        ''' '''
        res =  super(GistView,self).get_form_kwargs()
        #: ここで強制的にデータを変換させたりとか 
        return res

    def get_form(self, form_class): 
        ''' 
        '''
        vals =  self.get_form_kwargs()   #:GET,POST & FILES
        vals['initial'] = {'user': self.request.user, }
        if self.kwargs['command'] != None:
            form_class = GistItemForm

        try: 
            ret = form_class( ** vals )
            return ret
        except Exception,e:
#            log.debug(str(e))	
            print str(e)
            raise e

        return form_class()          #:TODO: should be error?      

    def get_context_data(self, **kwargs):
        ''' コンテキストデータ '''
        ret = super(GistView,self).get_context_data(**kwargs)
        #: 追加はここで
        form = kwargs.get('form',None)
        if form and form.instance and form.instance.response:
            ret['gist_response'] = form.instance.response.json 
        return ret
    
    def form_valid(self, form):
        ''' ''' 
        try:
            action = self.request.POST.get('action',u'edit') 
            form.instance.user = self.request.user
            { 
                "save": lambda : form.save(),
                "edit": lambda : True,
                "preview": lambda : form.to_preview(),
            }[action](); 
            
        except Exception,e:
            action = "edit"
            print str(e)

        self.template_name = "github/gist/%s.html" % action
                
        return self.render_to_response(self.get_context_data(
                        form = form
                    ))
        
    def form_invalid(self, form):
        return super(GistView ,self).form_invalid(form)

