# -*- coding: utf-8 -*-

from django.test import TestCase
import os

class GitTest(TestCase):
    def test_gist_list_simple(self):

        token = os.environ.get('GITHUB_TEST_TOKEN',None)
        self.assertIsNotNone(token)
        
        from app.github.api import Gist 
        gist = Gist(access_token=token)
        res = gist.list()
        for g in res.json:          #: response is list
            print g['description'], g['public'],g['user']['login'],g['comments']   
            print g

    def test_gist_create_simple(self):

        token = os.environ.get('GITHUB_TEST_TOKEN',None)
        self.assertIsNotNone(token)
        
        from app.github.api import Gist 
        gist = Gist(access_token=token)
        gist.add_file('test.py','import os')
        res = gist.create()
        print res.json
        print res.error,res.status_code
        print res.headers
        print res.request.url,res.request.headers
        print dir( res.request )
#        print dir(res)

class GistViewTest(TestCase):

    fixtures=['auth.json','social_auth',] 
    token = os.environ.get('GITHUB_TEST_TOKEN',None)

    def setUp(self):
        from django.contrib.auth.models import User
        from social_auth.models import UserSocialAuth

        self.assertEqual(UserSocialAuth.objects.count(),1)
        usa=UserSocialAuth.objects.all()[0]
        self.assertEqual( usa.extra_data['access_token'], 'GITHUB_TEST_TOKEN')

        #: Update with Actual Token
        usa.extra_data['access_token'] = self.token
        usa.save()
        
        #: Check Updated Token
        usa=UserSocialAuth.objects.all()[0]
        self.assertEqual( usa.extra_data['access_token'], self.token)

        self.test_user = usa.user

    def test_gist_create_simple(self):
        from app.github.models import Gist

        g=Gist(user=self.test_user, filename="hoge.py", contents="xxxxxxxx" , ) 
        self.assertEqual(g.social_auth.extra_data['access_token'],self.token )
        g.save()
        self.assertEqual(g.response.status_code,201)
#        print g.response,g.response.json, type(g.response)
