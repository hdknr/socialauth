# -*- coding: utf-8 -*- from django import forms

from django import forms
from django.conf import settings
from django.core.files import File
from django.core.paginator import Paginator

from models import *

import os
import uuid
import re

class GistItemForm(forms.ModelForm):

    def __init__(self,*args,**kwargs):
        super(GistItemForm,self).__init__(*args,**kwargs)

    def save(self,*args,**kwargs):
        try:
            super(GistItemForm,self).save(*args,**kwargs )
        except:
            self.instance.save()

    def to_preview(self):
        ''' フォームのアイテムを全てhiddeに変更する 
        '''
        for k,f in self.fields.items():
            if str(type(f)).find('Multi') >0: 
                f.widget = forms.MultipleHiddenInput() 

            elif isinstance(f.widget, forms.HiddenInput ) == False:
                f.widget = forms.HiddenInput() 

    class Meta:
        model= Gist
        exclude = ['gid','user',]
        widgets = {
            'usage': forms.RadioSelect(),
        }

class GistListForm(forms.Form):
    pass

