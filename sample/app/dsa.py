# -*- coding: utf-8 -*-
# django-social-auth configuration

import os

# http://django-social-auth.readthedocs.org/en/latest/backends/github.html
#
GITHUB_APP_ID      = os.environ.get('GITHUB_CLIENT_ID','__TODO__IN_WSGI.PY__')
GITHUB_API_SECRET  = os.environ.get('GITHUB_CLIENT_SECRET','__TODO__IN_WSGI.PY__')
GITHUB_EXTENDED_PERMISSIONS = ["gist",]                #: list:OAuth AuthReqのscopeに指定されます

#
SOCIAL_AUTH_CREATE_USERS          = True
SOCIAL_AUTH_FORCE_RANDOM_USERNAME = False
SOCIAL_AUTH_DEFAULT_USERNAME      = 'socialauth_user'
SOCIAL_AUTH_COMPLETE_URL_NAME     = 'socialauth_complete'
#
SOCIAL_AUTH_PIPELINE = (

    'social_auth.backends.pipeline.social.social_auth_user',
    'social_auth.backends.pipeline.misc.save_status_to_session',

    'app.github.views.pipeline_binding',

    'social_auth.backends.pipeline.user.get_username',
    'social_auth.backends.pipeline.user.create_user',
    'social_auth.backends.pipeline.social.associate_user',
    'social_auth.backends.pipeline.social.load_extra_data',
    'social_auth.backends.pipeline.user.update_user_details',
)
